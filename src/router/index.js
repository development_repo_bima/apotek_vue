import Vue from 'vue'
import Router from 'vue-router'

// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')

// Views
const Dashboard = () => import('@/views/Dashboard')

// const Colors = () => import('@/views/theme/Colors')
// const Typography = () => import('@/views/theme/Typography')

// const Charts = () => import('@/views/Charts')
// const Widgets = () => import('@/views/Widgets')

// Views - Components
// const Cards = () => import('@/views/base/Cards')
// const Forms = () => import('@/views/base/Forms')
// const Switches = () => import('@/views/base/Switches')
// const Tables = () => import('@/views/base/Tables')
// const Tabs = () => import('@/views/base/Tabs')
// const Breadcrumbs = () => import('@/views/base/Breadcrumbs')
// const Carousels = () => import('@/views/base/Carousels')
// const Collapses = () => import('@/views/base/Collapses')
// const Jumbotrons = () => import('@/views/base/Jumbotrons')
// const ListGroups = () => import('@/views/base/ListGroups')
// const Navs = () => import('@/views/base/Navs')
// const Navbars = () => import('@/views/base/Navbars')
// const Paginations = () => import('@/views/base/Paginations')
// const Popovers = () => import('@/views/base/Popovers')
// const ProgressBars = () => import('@/views/base/ProgressBars')
// const Tooltips = () => import('@/views/base/Tooltips')

// Views - Buttons
// const StandardButtons = () => import('@/views/buttons/StandardButtons')
// const ButtonGroups = () => import('@/views/buttons/ButtonGroups')
// const Dropdowns = () => import('@/views/buttons/Dropdowns')
// const BrandButtons = () => import('@/views/buttons/BrandButtons')

// Views - Icons
// const Flags = () => import('@/views/icons/Flags')
// const FontAwesome = () => import('@/views/icons/FontAwesome')
// const SimpleLineIcons = () => import('@/views/icons/SimpleLineIcons')
// const CoreUIIcons = () => import('@/views/icons/CoreUIIcons')

// Views - Notifications
// const Alerts = () => import('@/views/notifications/Alerts')
// const Badges = () => import('@/views/notifications/Badges')
// const Modals = () => import('@/views/notifications/Modals')

// Views - Pages
// const Page404 = () => import('@/views/pages/Page404')
// const Page500 = () => import('@/views/pages/Page500')
// const Login = () => import('@/views/pages/Login')
// const Register = () => import('@/views/pages/Register')

const Auth = () => import('@/views/login/Login')

// Users
const Users = () => import('@/views/users/Users')
const User = () => import('@/views/users/User')

// Blogs
const Blogs = () => import('@/views/blog/Blogs')
const Vlogs = () => import('@/views/vlog/Vlogs')
const Blog = () => import('@/views/blog/UpdateBlog')

// Profil
const Profil = () => import('@/views/profil/Profil')

// Obat
const Obat = () => import('@/views/obat/Obat')

const Stok = () => import('@/views/stok/Stok')
const StokForm = () => import('@/views/stok/StokForm')

const Purchase = () => import('@/views/purchase/Purchase')
const PurchaseForm = () => import('@/views/purchase/PurchaseForm')
const Sales = () => import('@/views/sales/Sales')
const SalesForm = () => import('@/views/sales/SalesForm')

const ReturSales = () => import('@/views/retur/ReturSales')
const ReturPurchase = () => import('@/views/retur/ReturPurchase')

const IndexReport = () => import('@/views/report/IndexReport')
const PurchaseReport = () => import('@/views/report/PurchaseReport')
const SalesReport = () => import('@/views/report/SalesReport')
const StockReport = () => import('@/views/report/StockReport')
const FinanceReport = () => import('@/views/report/FinanceReport')
const DebtReport = () => import('@/views/report/DebtReport')
const DebtReportDetail = () => import('@/views/report/DebtReportDetail')
const ProfitReport = () => import('@/views/report/ProfitReport')
const NeracaReport = () => import('@/views/report/NeracaReport')

Vue.use(Router)

export default new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: DefaultContainer,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'profil',
          name: 'Update Profil',
          component: Profil
        },
        {
          path: 'kemasan',
          name: 'Kemasan',
          component: {
            render (c) { return c('router-view') }
          }, 
          children:[
            {
              path: '',
              component: Blogs
            },
            {
              path: ':id',
              meta: { label: 'Blog Details'},
              name: 'Blog',
              component: Blog,
            },
          ]
        },
        {
          path: 'supplier',
          name: 'Supplier',
          component: Vlogs
        },
        {
          path: 'obat',
          name: 'Obat',
          component: Obat
        },
        {
          path: 'stok',
          name: 'Stok',
          redirect: '/stok/index',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'index',
              name: 'Stok Opname',
              component: Stok
            },
            {
              path: 'form',
              name: 'Form Tambah Stok',
              component: StokForm
            },
            {
              path: 'form/:id',
              name: 'Lihat Stok',
              component: StokForm
            }
          ]
        },
        {
          path: 'purchase',
          redirect: '/purchase/index',
          name: 'Purchase',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'index',
              name: 'Transaksi Pembelian',
              component: Purchase
            },
            {
              path: 'form',
              name: 'Form Pembelian',
              component: PurchaseForm
            },
            {
              path: 'form/:id',
              name: 'Form Pembelian',
              component: PurchaseForm
            }
          ]
        },
        {
          path: 'sales',
          redirect: '/sales/index',
          name: 'Sales',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'index',
              name: 'Transaksi Penjualan',
              component: Sales
            },
            {
              path: 'form',
              name: 'Form Penjualan',
              component: SalesForm
            },
            {
              path: 'form/:id',
              name: 'Form Penjualan',
              component: SalesForm
            }
          ]
        },
        {
          path: 'retur',
          redirect: '/retur/retur-sales',
          name: 'Retur',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'retur-sales',
              name: 'Retur Penjualan',
              component: ReturSales
            },
            {
              path: 'retur-purchase',
              name: 'Retur Pembelian',
              component: ReturPurchase
            }
          ]
        },
        {
          path: 'report',
          redirect: '/report/report-index',
          name: 'Report',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'report-index',
              name: 'Form Generate Laporan',
              component: IndexReport
            },
            {
              path: 'report-keuangan',
              name: 'Form Laporan Keuangan',
              component: FinanceReport
            },
            {
              path: 'report-neraca',
              name: 'Form Laporan Neraca',
              component: NeracaReport
            },
            {
              path: 'report-hutang/:param',
              name: 'Laporan Hutang',
              component: DebtReport
            },
            {
              path: 'report-hutang-detail/:param',
              name: 'Laporan Detail Hutang',
              component: DebtReportDetail
            },
            {
              path: 'report-profit/:param',
              name: 'Laporan Laba/Rugi',
              component: ProfitReport
            },
            {
              path: 'report-sales/:param',
              name: 'Laporan Penjualan',
              component: SalesReport
            },
            {
              path: 'report-purchase/:param',
              name: 'Laporan Pembelian',
              component: PurchaseReport
            },
            {
              path: 'report-stock/:param',
              name: 'Laporan Stok',
              component: StockReport
            }
          ]
        },
        
        {
          path: 'users',
          meta: { label: 'Users'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              component: Users,
            },
            {
              path: ':id',
              meta: { label: 'User Details'},
              name: 'User',
              component: User,
            },
          ]
        }
      ]
    },
    {
      path: '/auth',
      name: 'Auth',
      component: Auth
    },
    
  ]
})
