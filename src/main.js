// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'core-js/es6/promise'
import 'core-js/es6/string'
import 'core-js/es7/array'
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import VueNumeric from 'vue-numeric'
import vSelect from 'vue-select'
import VuePellEditor from 'vue-pell-editor'
import VueSweetalert2 from 'vue-sweetalert2'
import JsonExcel from 'vue-json-excel'
// import vSelect from './components/Select.vue'

Vue.use(VueSweetalert2)
Vue.use(VueResource)
Vue.use(VueNumeric)
Vue.use(VuePellEditor)
Vue.use(BootstrapVue)

Vue.component('v-select', vSelect)
// Vue.component("v-select", VueSelect.VueSelect);
Vue.component('downloadExcel', JsonExcel)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  }
})
