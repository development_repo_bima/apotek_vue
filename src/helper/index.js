import Vue from 'vue'
import VueResource from 'vue-resource'
import router from '@/router/index.js'
import VueSweetalert2 from 'vue-sweetalert2'
import VueLocalStorage from 'vue-localstorage'
 
Vue.use(VueLocalStorage)

Vue.use(VueSweetalert2)
Vue.use(VueResource)

export default {
	satuan : [],
	authVerify: function(statusCode = false, lastRoute = 'dashboard'){
		if(!localStorage.getItem('token')){
	        Vue.swal({
	          type: 'error',
	          title: 'Session Expired!',
	          text: 'Please Re-login'
	        });
	        router.push({path: "/auth"});
	        localStorage.setItem('lastRoute', lastRoute);
      	}else if(statusCode){
	        localStorage.removeItem('token');
	        router.push({path: "/auth"});
	        localStorage.setItem('lastRoute', lastRoute);
      	}
	},

	formatMonth(date, full = true){
      let arr = ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des'];
      let arr_full = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

      let split = date.split("-");
      if(full){
        return split[2]+' '+arr[parseInt(split[1])]+' '+split[0];
      }else{
        return arr_full[parseInt(split[1])]+' '+split[0];
      }
    },

    get_satuan(){
      Vue.http.get('http://localhost/myapi2/public/api/get_satuan', {
        headers: this.headers
      }).then(function(res){
        if(res.data.status){
          this.satuan = res.data.data;  
          // console.log(res.data.data)
          // return res.data.data;
        }
      })
    },

  // tokenizing: localStorage.token,
  // tokenizing: Vue.localStorage.get('token'),

	headers: {
    'Authorization': 'Bearer ' +Vue.localStorage.get('token') 
  },

  formatPrice: function(value){
      let val = (value/1).toFixed(2).replace('.', ',')
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
  },

  url_prefix : 'http://localhost/myapi2/public/api/',
}