export default {

  // items: [
  //   {
  //     name: 'Dashboard',
  //     url: '/dashboard',
  //     icon: 'icon-home',
  //   },
  //   {
  //     name: 'Master Data',
  //     url:'#',
  //     icon: 'icon-social-dropbox',
  //     children: [
  //       {
  //         name: 'Master Kemasan',
  //         url: '/Kemasan',
  //         icon: 'icon-badge',
  //       },
  //       {
  //         name: 'Master Supplier',
  //         url: '/supplier',
  //         icon: 'icon-refresh',
  //       },
  //       {
  //         name: 'Master Obat',
  //         url: '/obat',
  //         icon: 'icon-chemistry',
  //       }
  //     ]
  //   },
  //   {
  //     name: 'Transaksi',
  //     url:'#',
  //     icon: 'icon-basket-loaded',
  //     children: [
  //       {
  //         name: 'Transaksi Pembelian',
  //         url: '/purchase/index',
  //         icon: 'icon-tag',
  //       },
  //       {
  //         name: 'Transaksi Penjualan',
  //         url: '/sales/index',
  //         icon: 'icon-basket',
  //       }
  //     ]
  //   },
  //   {
  //     name: 'Retur',
  //     url: '/retur',
  //     icon: 'icon-action-undo',
  //     children: [
  //       {
  //         name: 'Pembelian',
  //         url: '/retur/retur-purchase',
  //         icon: 'icon-directions'
  //       },
  //       {
  //         name: 'Penjualan',
  //         url: '/retur/retur-sales',
  //         icon: 'icon-directions'
  //       }
  //     ]
  //   },
  //   {
  //     name: 'Stok Barang',
  //     url: '/stok',
  //     icon: 'icon-info',
  //   },
  //   {
  //     name: 'Laporan',
  //     url: '/report/report-index',
  //     icon: 'icon-docs',
  //   }
  // ],

  item_admin: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-home',
    },
    {
      name: 'Master Data',
      url:'#',
      icon: 'icon-social-dropbox',
      children: [
        {
          name: 'Master Kemasan',
          url: '/Kemasan',
          icon: 'icon-badge',
        },
        {
          name: 'Master Supplier',
          url: '/supplier',
          icon: 'icon-refresh',
        },
        {
          name: 'Master Obat',
          url: '/obat',
          icon: 'icon-chemistry',
        }
      ]
    },
    {
      name: 'Transaksi',
      url:'#',
      icon: 'icon-basket-loaded',
      children: [
        {
          name: 'Transaksi Pembelian',
          url: '/purchase/index',
          icon: 'icon-tag',
        },
        {
          name: 'Transaksi Penjualan',
          url: '/sales/index',
          icon: 'icon-basket',
        }
      ]
    },
    {
      name: 'Retur',
      url: '/retur',
      icon: 'icon-action-undo',
      children: [
        {
          name: 'Pembelian',
          url: '/retur/retur-purchase',
          icon: 'icon-directions'
        },
        {
          name: 'Penjualan',
          url: '/retur/retur-sales',
          icon: 'icon-directions'
        }
      ]
    },
    {
      name: 'Stok Barang',
      url: '/stok',
      icon: 'icon-info',
    },
    {
      name: 'Laporan',
      url: '/report',
      icon: 'icon-docs',
      children: [
        {
          name: 'Laporan Sales',
          url: '/report/report-index',
          icon: 'icon-docs'
        },
        {
          name: 'Laporan Keuangan',
          url: '/report/report-keuangan',
          icon: 'icon-docs'
        },
        {
          name: 'Laporan Neraca',
          url: '/report/report-neraca',
          icon: 'icon-docs'
        }
      ]
    }
  ],

  item_asisten: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-home',
    },
    {
      name: 'Transaksi',
      url:'#',
      icon: 'icon-basket-loaded',
      children: [
        {
          name: 'Transaksi Pembelian',
          url: '/purchase/index',
          icon: 'icon-tag',
        },
        {
          name: 'Transaksi Penjualan',
          url: '/sales/index',
          icon: 'icon-basket',
        }
      ]
    },
    {
      name: 'Retur',
      url: '/retur',
      icon: 'icon-action-undo',
      children: [
        {
          name: 'Penjualan',
          url: '/retur/retur-sales',
          icon: 'icon-directions'
        }
      ]
    },
    {
      name: 'Stok Barang',
      url: '/stok',
      icon: 'icon-info',
    }
  ]
}
